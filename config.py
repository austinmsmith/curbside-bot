#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os

class DefaultConfig:
    """ Bot Configuration """

    PORT = 8081
    APP_ID = ""
    APP_PASSWORD = ""
    INSTALL_URL = "https://dciostaging.synqtech.com/api/v1/integrations/teams/botinstall"
    CLAIM_URL = "https://dciostaging.synqtech.com/api/v1/arrivals/claim"
    LOGO_URL = "https://dciostaging.synqtech.com/img/synqlogo_icon.png"