import re


def conversation_to_channel(conversation_id):
    channel_id = re.findall(r"(.*);messageid=.*", conversation_id)
    if len(channel_id) == 0:
        raise ValueError("The conversation id was invalid. No message_id could be found.")
    return channel_id[0]

def conversation_to_message(conversation_id):
    message_id = re.findall(r".*;messageid=(.+)", conversation_id)
    if len(message_id) == 0:
        raise ValueError("The conversation id was invalid. No message_id could be found.")
    return message_id[0]


def build_conversation_id(message_id, channel_id):
    return channel_id + ";messageid=" + str(message_id)