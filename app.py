# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import sys
import traceback
import uuid
import argparse
from datetime import datetime
from typing import Dict
from aiohttp import web
from aiohttp.web import Request, Response, json_response
from botbuilder.core import (
    BotFrameworkAdapterSettings,
    TurnContext,
    BotFrameworkAdapter,
    MessageFactory,
    CardFactory
)
from botbuilder.core.integration import aiohttp_error_middleware
from botbuilder.schema import Activity, ActivityTypes, ConversationReference, ConversationParameters

from bot import MyBot
from config import DefaultConfig
from helpers import build_conversation_id

CONFIG = DefaultConfig()

# Create adapter.
# See https://aka.ms/about-bot-adapter to learn more about how bots work.
SETTINGS = BotFrameworkAdapterSettings(CONFIG.APP_ID, CONFIG.APP_PASSWORD)
ADAPTER = BotFrameworkAdapter(SETTINGS)


# Catch-all for errors.
async def on_error(context: TurnContext, error: Exception):
    # This check writes out errors to console log .vs. app insights.
    # NOTE: In production environment, you should consider logging this to Azure
    #       application insights.
    print(f"\n [on_turn_error] unhandled error: {error}", file=sys.stderr)
    traceback.print_exc()

    # Send a message to the user
    await context.send_activity("The bot encountered an error or bug.")
    await context.send_activity(
        "To continue to run this bot, please fix the bot source code."
    )
    # Send a trace activity if we're talking to the Bot Framework Emulator
    if context.activity.channel_id == "emulator":
        # Create a trace activity that contains the error object
        trace_activity = Activity(
            label="TurnError",
            name="on_turn_error Trace",
            timestamp=datetime.utcnow(),
            type=ActivityTypes.trace,
            value=f"{error}",
            value_type="https://www.botframework.com/schemas/error",
        )
        # Send a trace activity, which will be displayed in Bot Framework Emulator
        await context.send_activity(trace_activity)


ADAPTER.on_turn_error = on_error

# Create the Bot
CONVERSATION_REFERENCES: Dict[str, ConversationReference] = dict()

BOT = MyBot(CONVERSATION_REFERENCES)

APP_ID = SETTINGS.app_id if SETTINGS.app_id else uuid.uuid4()


# Listen for incoming requests on /api/messages
async def messages(req: Request) -> Response:
    # Main bot message handler.
    if "application/json" in req.headers["Content-Type"]:
        body = await req.json()
    else:
        return Response(status=415)

    activity = Activity().deserialize(body)
    print(activity.channel_data["team"]["id"])
    auth_header = req.headers["Authorization"] if "Authorization" in req.headers else ""
    print("+++++++++++++++++++++++++++++++++++++++++++++++++")
    print(auth_header)
    print("+++++++++++++++++++++++++++++++++++++++++++++++++")
    print(activity)
    print(activity.from_property)

    try:
        response = await ADAPTER.process_activity(activity, auth_header, BOT.on_turn)
        if response:
            return json_response(data=response.body, status=response.status)
        return Response(status=201)
    except Exception as exception:
        raise exception

async def arrival(req: Request) -> Response:
    print("Customer has arrived......................................................")
    data = await req.json()
    print(data)
    response = await _send_arrival_message(data)
    return json_response(data=response, status=201) 

async def _send_arrival_message(req):
    attach, raw = BOT.create_adaptive_card_attachment("cards/arrival_card.json", req)
    print("debug1")
    message = MessageFactory.attachment(attach)
    print("debug2")
    params = ConversationParameters(
                                    is_group=True, 
                                    channel_data={"channel": {"id": req['channel_id']}},
                                    activity=message
                                    )
    print("debug3")

    connector = await ADAPTER.create_connector_client(req["service_url"])
    print("debug4")

    conversation_resource = await connector.conversations.create_conversation(params)
    print("debug5")
    print(conversation_resource)
    print(raw)
    BOT.register_active_arrival(conversation_resource.activity_id, raw)
    print(conversation_resource)
    return {"message_id": conversation_resource.activity_id}

async def order(req: Request) -> Response:
    print("Customer order has been requested......................................................")
    data = await req.json()
    print(data)
    print("debug-2")
    attach, raw = BOT.create_order_attachment("cards/order_card.json", data)
    print("debug-1")
    message = MessageFactory.attachment(attach)
    print("debug0")
    params = ConversationParameters(
                                    is_group=True, 
                                    channel_data={"channel": {"id": data['channel_id']}},
                                    activity=message
                                    )
    print("debug1")
    connector = await ADAPTER.create_connector_client(data["service_url"])
    print("debug2")
    conversation_resource = await connector.conversations.create_conversation(params)
    print("debug3")
    BOT.register_active_arrival(conversation_resource.activity_id, raw)
    return json_response(status=201, data={"message_id": conversation_resource.activity_id})

async def escalation(req: Request) -> Response:
    print("Customer arrival has been escalated......................................................")
    print(req.headers)
    print(await req.text())
    data = await req.json()
    print(data)
    print("debug-2")
    attach, raw = BOT.create_escalation_attachment("cards/escalation_card.json", data)
    print("debug-1")
    message = MessageFactory.attachment(attach)
    print("debug0")
    params = ConversationParameters(
                                    is_group=True, 
                                    channel_data={"channel": {"id": data['channel_id']}},
                                    activity=message
                                    )
    print("debug1")
    connector = await ADAPTER.create_connector_client(data["service_url"])
    print("debug2")
    conversation_resource = await connector.conversations.create_conversation(params)
    print("debug3")
    BOT.register_active_arrival(conversation_resource.activity_id, raw)
    return json_response(status=201, data={"message_id": conversation_resource.activity_id})

async def update_conversation(req: Request) -> Response:
    print("bruh")
    data = await req.json()
    print(data)
    connector = await ADAPTER.create_connector_client(data["service_url"])
    print(connector)
    conversation_id = build_conversation_id(data["message_id"], data["channel_id"])
    print(conversation_id)
    if data["status"] == "deleted":
        await connector.conversations.delete_activity(conversation_id, data["message_id"])
        success_text = "Deletion Successful"
    
    elif data["status"] == "claimed":
        attach, old_card = BOT.create_adaptive_card_attachment("cards/arrival_card.json", data)
        new_card = BOT.format_claimed_card(old_card, data["claimed_by"])
        message = MessageFactory.attachment(CardFactory.adaptive_card(new_card))
        message.id = data["message_id"]
        update = await connector.conversations.update_activity(conversation_id, data["message_id"], message)
        success_text = "Claim Updated"

    elif data["status"] == "delivered":
        card = BOT.create_delivered_attachment("cards/delivery_card.json", data)
        message = MessageFactory.attachment(card)
        message.id = data["message_id"]
        update = await connector.conversations.update_activity(conversation_id, data["message_id"], message)
        success_text = "Delivery Updated"
    
    else:
        return Response(status=400, text="Status unrecognized")

    return Response(status=201, text=success_text)

APP = web.Application(middlewares=[aiohttp_error_middleware])
APP.router.add_post("/api/messages", messages)
APP.router.add_post("/api/arrival", arrival)
APP.router.add_post("/api/order", order)
APP.router.add_post("/api/escalation", escalation)
APP.router.add_post("/api/update_conversation", update_conversation)

parser = argparse.ArgumentParser(description="curbside bot")
parser.add_argument('--path')
parser.add_argument('--port')

if __name__ == "__main__":
    try:
        args = parser.parse_args()
        web.run_app(APP, path=args.path, port=args.port)
#web.run_app(APP, host="localhost", port=CONFIG.PORT)
    except Exception as error:
        raise error
