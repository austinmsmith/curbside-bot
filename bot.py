# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os
import json
import aiohttp
import re
from typing import Dict
from helpers import conversation_to_message, conversation_to_channel
from botbuilder.core.teams import TeamsInfo
from botbuilder.core import ActivityHandler, TurnContext, CardFactory, MessageFactory
from botbuilder.schema import Activity, ActivityTypes, Attachment, ConversationReference, ChannelAccount 
from botbuilder.schema.teams import TeamDetails
from config import DefaultConfig

class MyBot(ActivityHandler):
    # See https://aka.ms/about-bot-activity-message to learn more about the message and other activity types.

    def __init__(self, conversation_references: Dict[str, ConversationReference]):
        self.config = DefaultConfig()
        self.conversation_references = conversation_references
        self.init_url = self.config.INSTALL_URL
        self.claim_url = self.config.CLAIM_URL
        self.active_arrivals = {}

    async def on_message_activity(self, turn_context: TurnContext):
        # self._add_conversation_reference(turn_context.activity)
        # print(turn_context.activity)
        await turn_context.send_activity(f"Message recieved!")

    async def on_event(self, turn_context):
        print("event====================================================")
        return super().on_event(turn_context)

    async def on_event_activity(self, turn_context):
        print("event_activity====================================================")
        return super().on_event_activity(turn_context)
    
    async def on_invoke_activity(self, turn_context):
        print("invoke====================================================")
        name = turn_context.activity.from_property.name
        # -------------------------------------------
        # Claim
        # -------------------------------------------

        # Cached in local memory version (OLD)
        # old_card = self.active_arrivals[str(conversation_to_message(turn_context.activity.conversation.id))]
    
        # Hitting claim endpoint version (NEW)
        message_id = conversation_to_message(turn_context.activity.conversation.id)
        channel_id = conversation_to_channel(turn_context.activity.conversation.id)
        claim_json = {
            "message_id": message_id,
            "channel_id": channel_id,
            "claimed_by": name
        }
        async with aiohttp.ClientSession() as session:
            async with session.post(self.claim_url, json=claim_json) as r:
                params = await r.json()
        print(params)
        old_card = self.create_adaptive_card_attachment("cards/arrival_card.json",params)
        print(old_card)  
        new_card = self.format_claimed_card(old_card[1], params["claimed_by"])
        message = MessageFactory.attachment(CardFactory.adaptive_card(new_card))
        message.id = turn_context.activity.reply_to_id
        update = await turn_context.update_activity(message)

    async def on_members_added_activity(
        self,
        members_added: ChannelAccount,
        turn_context: TurnContext
    ):
        print(members_added[0])
        team_details = await TeamsInfo.get_team_details(turn_context)
        print("+++++++++++++++++++++++++++++++++++++++")
        print(team_details)
        channels = await TeamsInfo.get_team_channels(turn_context)
        channel_info = [{"channel_id": channel.id, "channel_name": channel.name if channel.name is not None else "General"} for channel in channels]
        pickup_json = {
            "tab_entity_id": "88888888888888888888888888",
            "channels": channel_info,
            "team_id": team_details.id,
            "team_name": team_details.name,
            "service_url": turn_context.activity.service_url,
            "tenant_id": team_details.additional_properties["tenantId"]
        }
        print(pickup_json)
        async with aiohttp.ClientSession() as session:
            print(await session.post(self.init_url, json=pickup_json))
        
        for member_added in members_added:
            if member_added.id != turn_context.activity.recipient.id:
                await turn_context.send_activity("Hello and welcome!")

    # This is just temporary to get a chat id
    # to be replaced once we can get the channel id to post in for teams

    def _add_conversation_reference(self, activity: Activity):
        conversation_reference = TurnContext.get_conversation_reference(activity)
        self.conversation_references[
            conversation_reference.user.id
        ] = conversation_reference
    
    def create_adaptive_card_attachment(self, path, params) -> Attachment:
        p = os.path.join(os.path.dirname(os.path.abspath(__file__)), path)
        
        with open(p, "rb") as in_file:
            card_data = json.load(in_file)
        
        self._format_adaptive_card(card_data, params)
        return CardFactory.adaptive_card(card_data), card_data
    
    def create_order_attachment(self, path, params):
        p = os.path.join(os.path.dirname(os.path.abspath(__file__)), path)
        with open(p, "rb") as in_file:
            card = json.load(in_file)
        card["body"][0]["items"][1]["columns"][0]["items"][0]["url"] = self.config.LOGO_URL
        card["body"][0]["items"][1]["columns"][1]["items"][0]["text"] = f"{params['name']} {params['phone_number']}"
        card["body"][0]["items"][1]["columns"][1]["items"][1]["text"] = f"Created {params['date']}"
        card["body"][0]["items"][2]["items"][0]["url"] = params["product_image_url"]
        card["body"][1]["items"][0]["facts"][0]["value"] = str(params["order_number"])
        card["actions"][0]["url"] = str(params["order_url"])
        card["actions"][1]["url"] = str(params["other_orders_url"])
        return CardFactory.adaptive_card(card), card

    def create_escalation_attachment(self, path, params) -> Attachment:
        p = os.path.join(os.path.dirname(os.path.abspath(__file__)), path)
        with open(p, "rb") as in_file:
            card = json.load(in_file)
        card["body"][0]["items"][0]["text"] = f"Arrival Unreviewed for {params['timeout_minutes']} Minutes"
        card["body"][0]["items"][1]["columns"][0]["items"][0]["url"] = self.config.LOGO_URL
        card["body"][0]["items"][1]["columns"][1]["items"][0]["text"] = f"{params['name']} {params['phone_number']}"
        card["body"][0]["items"][1]["columns"][1]["items"][1]["text"] = f"Created {params['date']}"
        card["body"][1]["items"][0]["facts"][0]["value"] = str(params["order_number"])
        card["body"][1]["items"][0]["facts"][1]["value"] = str(params["pickup_code"])
        card["body"][1]["items"][0]["facts"][2]["value"] = str(params["parking_stall"])
        card["actions"][0]["url"] = str(params["order_url"])
        card["actions"][1]["url"] = str(params["other_orders_url"])
        
        return CardFactory.adaptive_card(card), card

    def _format_adaptive_card(self, card, params):
        card["body"][0]["items"][1]["columns"][0]["items"][0]["url"] = self.config.LOGO_URL
        card["body"][0]["items"][1]["columns"][1]["items"][0]["text"] = f"{params['name']} {params['phone_number']}"
        card["body"][0]["items"][1]["columns"][1]["items"][1]["text"] = f"Created {params['date']}"
        card["body"][1]["items"][0]["facts"][0]["value"] = str(params["order_number"])
        card["body"][1]["items"][0]["facts"][1]["value"] = str(params["pickup_code"])
        card["body"][1]["items"][0]["facts"][2]["value"] = str(params["parking_stall"])
        card["actions"][1]["url"] = str(params["order_url"])
        card["actions"][2]["url"] = str(params["other_orders_url"])
    
    def format_claimed_card(self, card, name):
        card["body"][0]["items"][0]["text"] = "Arrival Claimed by " + str(name)
        card["body"][0]["style"] = "good"
        card["actions"].pop(0)
        return card
    
    def create_delivered_attachment(self, path, params):
        p = os.path.join(os.path.dirname(os.path.abspath(__file__)), path)
        with open(p, "rb") as in_file:
            card = json.load(in_file)
        card["body"][0]["items"][0]["text"] = f"Arrival (Pickup code {params['pickup_code']}) delivered by {params['claimed_by']}."
        return CardFactory.adaptive_card(card)


    def register_active_arrival(self, activity_id, card):
        self.active_arrivals[str(activity_id)] = card
        print(self.active_arrivals)
